# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class UserProfile(models.Model):
    uid = models.CharField(max_length=255)
    user_ip = models.CharField(max_length=255)
    user_agent = models.CharField(max_length=255)
    user_lang = models.CharField(max_length=255)
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.uid


class EventProfile(models.Model):
    action_name = models.CharField(max_length=250)
    user = models.ForeignKey(UserProfile, related_name="events")
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.action_name
