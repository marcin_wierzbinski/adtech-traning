# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.views.generic import View
from .models import UserProfile, EventProfile


class TrackerMain(View):

    def get(self, request):
        uid = request.GET.get('_id')
        ip = request.META.get('REMOTE_ADDR')
        user_agent = request.META.get('HTTP_USER_AGENT')
        user_lang = request.META.get('HTTP_ACCEPT_LANGUAGE')

        action_name = request.GET.get('action_name')

        if UserProfile.objects.filter(uid=uid).exists():
            u = UserProfile.objects.get(uid=uid)
        else:
            u = UserProfile(uid=uid, user_ip=ip, user_agent=user_agent, user_lang=user_lang)
            u.save()
        event = EventProfile(action_name=action_name, user=u)
        event.save()
        response = HttpResponse(status=204)
        response.set_cookie('_id', uid)


        return response
