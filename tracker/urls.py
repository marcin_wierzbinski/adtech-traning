from django.conf.urls import include, url
from .views import TrackerMain

urlpatterns = [
    url(r'^', TrackerMain.as_view()),
]
