# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import EventProfile, UserProfile

admin.site.register(UserProfile)
admin.site.register(EventProfile)
