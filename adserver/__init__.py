# -*- coding: utf-8 -*-
from collections import namedtuple

ProductInfo = namedtuple('ProductInfo', 'name price category_id category_name')


class Products:
    FACE_BEARD = ProductInfo("Full Beard", 13.99, 1, "FACE")
    FACE_GOATEE = ProductInfo("Goatee Beard", 13.99, 1, "FACE")
    FACE_MOUSTACHE = ProductInfo("Retro Moustache", 9.99, 1, "FACE")
    CONTACTS_BROWN = ProductInfo("Light Brown Colored Contact Lenses 2pcs", 4.99, 1, "FACE")
    CONTACTS_BLUE = ProductInfo("Blue Colored Contact Lenses 2pcs", 4.99, 1, "FACE")
    CONTACTS_DARK_BROWN = ProductInfo("Dark Brown Colored Contact Lenses 2pcs", 4.99, 1, "FACE")
    WIG_MEN_BROWN = ProductInfo("Men’s Short Brown Wig", 24.99, 2, "WIG")
    WIG_MEN_BLONDE = ProductInfo("Men’s Short Blonde Wig", 24.99, 2, "WIG")
    WIG_WOMEN_RED = ProductInfo("Women’s Long Red Wig", 39.99, 2, "WIG")
    WIG_WOMEN_BLONDE = ProductInfo("Women’s Long Blonde Wig", 39.99, 2, "WIG")
    COAT_LIGHT = ProductInfo("Classic Trench Coat – Beige", 89.99, 4, "CLOTHING")
    CLOAT_BLACK = ProductInfo("Classic Trench Coat – Black", 89.99, 4, "CLOTHING")
    CAMERA_GLASSES = ProductInfo("Video Camera Glasses", 149.99, 3, "CAMERA")
    CAMERA_SUNGLASSES = ProductInfo("Video Camera Sunglasses", 149.99, 3, "CAMERA")
    CAMERA_CUP = ProductInfo("Paper Coffee Cup With Hidden Video Cam", 79.99, 3, "CAMERA")
    CAMERA_KEYS = ProductInfo("Car Keys With Hidden Video Cam", 69.99, 3, "CAMERA")
    BINOCULARS = ProductInfo("Long Distance Binoculars", 119.99, 5, "ACCESSORIES")
    MAGNIFYING_GLASS = ProductInfo("Handheld Magnifying Glass", 19.99, 5, "ACCESSORIES")
    FEDORA = ProductInfo("Fedora Hat", 49.99, 5, "ACCESSORIES")

    @classmethod
    def product_info(cls, page_title):
        product_dict = {cls.__dict__[name].name: cls.__dict__[name] for name in cls.__dict__ if name.isupper()}
        product_name = page_title.split('|')[0].strip()  # Goatee Beard | Rainbow Store
        return product_dict[product_name]

    @classmethod
    def product_sku(cls, page_title):
        product_dict = {cls.__dict__[name].name: name for name in cls.__dict__ if name.isupper()}
        product_name = page_title.split('|')[0].strip()  # Goatee Beard | Rainbow Store
        return product_dict[product_name].replace('_', '-')
