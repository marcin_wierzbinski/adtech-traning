# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.views.generic import View
from tracker.models import UserProfile
from . import Products

class AdServer(View):

    SMALL = """
    <div style="position:relative;">
  <a href="http://shop.workshop.clearcode.cc/product_sku/{0}" target="_parent" style="display: block;">
    <div style="background-image: url('http://adserver.workshop.clearcode.cc/media/creatives/{0}.png'); width: 92px; height: 105px;background-position: center;background-size: contain;background-repeat: no-repeat;position: absolute;top: 68px;left: 41px;"/></div>
    <div style="position: absolute;top: 198px;left: 31px;font-family: helvetica;width: 112px;height:40px; text-align:center;">
      <span style="font-weight: bold; font-size: 11px; text-align: center;position: absolute;  top: 50%;  transform:translateY(-50%); width:100%; left:0; text-decoration:none !important; color:black;">{1}
      </span>
    </div>
    <img src="http://adserver.workshop.clearcode.cc/media/creatives/300x250_base.png"/>
  </a>
</div>
    """

    BIG = """
    <div style="position:relative;">
  <a href="http://shop.workshop.clearcode.cc/product_sku/{0}" target="_parent" style="display: block;">
    <div style="background-image: url('http://adserver.workshop.clearcode.cc/media/creatives/{0}.png'); width: 111px; height: 126px; background-position: center; background-size: contain; background-repeat: no-repeat; position: absolute;top: 158px; left: 24px;"/></div>
    <div style="position: absolute;top: 320px; left: 14px; font-family: helvetica; width: 132px; height:43px; text-align:center;">
      <span style="font-weight: bold; font-size: 11px; text-align: center; position: absolute; top: 50%; transform:translateY(-50%); width:100%; left:0; text-decoration:none !important; color:black;">{1}
      </span>
    </div>
    <img src="http://adserver.workshop.clearcode.cc/media/creatives/160x600_base.png"/>
  </a>
</div>
    
    """

    def get(self, request):
        uid = request.COOKIES.get('_id')
        height = request.GET.get('height')
        width = request.GET.get('width')
        user = UserProfile.objects.get(uid=uid)
        latest_event = user.events.latest("create_date")
        product_sku = Products.product_sku(latest_event.action_name)
        product_info = Products.product_info(latest_event.action_name)
        if int(height) + int(width) < 600:
            img = self.SMALL
        else:
            img = self.BIG
        response = HttpResponse(img.format(product_sku, product_info.name))
        response['X-Frame-Options'] = 'ALLOW_ALL'
        return response
