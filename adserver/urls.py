from django.conf.urls import url
from .views import AdServer

urlpatterns = [
    url(r'^', AdServer.as_view()),
]
